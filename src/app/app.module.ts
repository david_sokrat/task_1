import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { CheckDomainComponent } from './check-domain/check-domain.component';
import { DjinoPreferencesComponent } from './djino-preferences/djino-preferences.component';
import { DjinoInfoComponent } from './djino-info/djino-info.component';
import { NavDotsComponent } from './nav-dots/nav-dots.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateAccountComponent,
    CheckDomainComponent,
    DjinoPreferencesComponent,
    DjinoInfoComponent,
    NavDotsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
