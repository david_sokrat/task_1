import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-check-domain',
  templateUrl: './check-domain.component.html',
  styleUrls: ['./check-domain.component.less']
})
export class CheckDomainComponent implements OnInit {
  @ViewChild('curDomain') curDomain: ElementRef;

  public response: string;
  public available: boolean;
  domains = [
    {free: true, name: 'example.ru'},
    {free: false, name: '1example.ru'},
    {free: false, name: '2example.ru'},
    {free: false, name: '3example.ru'},
    {free: false, name: '4example.ru'}
  ];


  constructor() { }

  ngOnInit() {

  }

  getAvailableDomains() {
    const domain = this.curDomain.nativeElement.value;
    const index = this.domains.findIndex(x => x.name === domain);
    if (index === -1) {
      this.domains.push({free: true, name: domain});
    }

    const filteredDomain = this.domains.filter(obj => {
      return obj.name === domain;
    });

    if (filteredDomain[0].free === true) {
      this.response = 'Домен: ' + domain + ' - свободен. ';
    } else {
      this.response = 'Домен: ' + domain + ' - занят. ';
    }

    this.available = filteredDomain[0].free;
  }

  scrollDown() {
    $('html, body').animate({
      scrollTop: $('#djino_preferences').offset().top
    }, 1000, function() {
      $('.nav_item1').removeClass('active');
      $('.nav_item2').addClass('active');
      $('.nav_item3').removeClass('active');
    });
  }
}
