import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavDotsComponent } from './nav-dots.component';

describe('NavDotsComponent', () => {
  let component: NavDotsComponent;
  let fixture: ComponentFixture<NavDotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavDotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavDotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
