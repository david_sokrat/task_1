import { Component, OnInit, HostListener } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-nav-dots',
  templateUrl: './nav-dots.component.html',
  styleUrls: ['./nav-dots.component.less']
})
export class NavDotsComponent implements OnInit {
  @HostListener('window:scroll', [])
  checkElementsPosition() {
    const checkDomainHeight = $('#check_domain').height() / 2;
    const djinoPrefHeight = $('#djino_preferences').height() / 2 + checkDomainHeight;
    const djinoInfoHeight = $('#djino_info').height() / 2 + djinoPrefHeight;
    const elScrolled = window.scrollY;

    if (elScrolled < checkDomainHeight) {
      $('.nav_item1').addClass('active');
      $('.nav_item2').removeClass('active');
      $('.nav_item3').removeClass('active');
    } else if (elScrolled < djinoPrefHeight) {
      $('.nav_item1').removeClass('active');
      $('.nav_item2').addClass('active');
      $('.nav_item3').removeClass('active');
    } else if (elScrolled < djinoInfoHeight) {
      $('.nav_item1').removeClass('active');
      $('.nav_item2').removeClass('active');
      $('.nav_item3').addClass('active');
    }
  }

  constructor() { }

  ngOnInit() {
    this.checkElementsPosition();
  }

  scrollToCheckDomain() {
    $('html, body').animate({
      scrollTop: $('#check_domain').offset().top
    }, 1000, function() {
      $('.nav_item1').addClass('active');
      $('.nav_item2').removeClass('active');
      $('.nav_item3').removeClass('active');
    });
  }

  scrollToDjinoPreferences() {
    $('html, body').animate({
      scrollTop: $('#djino_preferences').offset().top
    }, 1000, function() {
      $('.nav_item1').removeClass('active');
      $('.nav_item2').addClass('active');
      $('.nav_item3').removeClass('active');
    });
  }

  scrollToDjinoInfo() {
    $('html, body').animate({
      scrollTop: $('#djino_info').offset().top
    }, 1000, function() {
      $('.nav_item1').removeClass('active');
      $('.nav_item2').removeClass('active');
      $('.nav_item3').addClass('active');
    });
  }

}
