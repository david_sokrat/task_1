import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjinoPreferencesComponent } from './djino-preferences.component';

describe('DjinoPreferencesComponent', () => {
  let component: DjinoPreferencesComponent;
  let fixture: ComponentFixture<DjinoPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjinoPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjinoPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
