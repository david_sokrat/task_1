import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjinoInfoComponent } from './djino-info.component';

describe('DjinoInfoComponent', () => {
  let component: DjinoInfoComponent;
  let fixture: ComponentFixture<DjinoInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjinoInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjinoInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
